package main

import (
	"context"
	//"encoding/json"
	"fmt"
	//"net/http"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
)

func handleGetGraphsFunc(devices []Device) GraphData {
	fmt.Println("Get devices!!!----")
	collection := client.Database(dbName).Collection(devices_colletion)

	// Define a slice to store retrieved facilities
	//var devices_green []Device

	//var devices_red []Device

	//var tag_1_devices []Device
	var tag_1_red_devices []Device
	var tag_1_green_devices []Device

	var tag_2_red_devices []Device
	var tag_2_green_devices []Device

	var tag_3_red_devices []Device
	var tag_3_green_devices []Device

	var tag_4_red_devices []Device
	var tag_4_green_devices []Device

	var tag_5_red_devices []Device
	var tag_5_green_devices []Device

	//var graphData []GraphData

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		fmt.Println("ERR", err)
		//http.Error(w, "Error retrieving devices", http.StatusInternalServerError)
		//return
	} //
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	// for cursor.Next(context.Background()) {
	// 	var device Device
	// 	if err := cursor.Decode(&device); err != nil {
	// 		//http.Error(w, "Error decoding device", http.StatusInternalServerError)

	// 		fmt.Println("ERR", err)
	// 		//return
	// 	}

	for _, device := range devices {
		if device.Name != "" && device.Name != "NaN" {

			//fmt.Println("TAG-", device.Name)

			if device.Name == "TAG-1:" {
				//fmt.Println("TAG-1", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						//http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						//return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_1_red_devices = append(tag_1_red_devices, device)
					} else if rssi < -50 {
						tag_1_green_devices = append(tag_1_green_devices, device)
					}
				}

			} else if device.Name == "TAG-2:" {
				//fmt.Println("TAG-2", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						//http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						//return err
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_2_red_devices = append(tag_2_red_devices, device)
					} else if rssi < -50 {
						tag_2_green_devices = append(tag_2_green_devices, device)
					}
				}

			} else if device.Name == "TAG-3:" {
				//fmt.Println("TAG-3", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						//http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						//return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_3_red_devices = append(tag_3_red_devices, device)
					} else if rssi < -50 {
						tag_3_green_devices = append(tag_3_green_devices, device)
					}
				}

			} else if device.Name == "TAG-4:" {
				//fmt.Println("TAG-4", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						//http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						//return
					} //

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_4_red_devices = append(tag_4_red_devices, device)
					} else if rssi < -50 {
						tag_4_green_devices = append(tag_4_green_devices, device)
					}
				}

			} else if device.Name == "TAG-5:" {
				//fmt.Println("TAG-5", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						//http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						//return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_5_red_devices = append(tag_5_red_devices, device)
					} else if rssi < -50 {
						tag_5_green_devices = append(tag_5_green_devices, device)
					}
				}

			}

		}

		//devices = append(devices, device)

		// Get the length of the slices

	}

	tag_1_greenLength := len(tag_1_green_devices)
	tag_1_redLength := len(tag_1_red_devices)

	tag_2_greenLength := len(tag_2_red_devices)
	tag_2_redLength := len(tag_2_green_devices)

	tag_3_greenLength := len(tag_3_red_devices)
	tag_3_redLength := len(tag_3_green_devices)

	tag_4_greenLength := len(tag_4_red_devices)
	tag_4_redLength := len(tag_4_green_devices)

	tag_5_greenLength := len(tag_5_red_devices)
	tag_5_redLength := len(tag_5_green_devices)

	fmt.Println("tag 1 Green Length:-", tag_1_greenLength)
	fmt.Println("tag 1 Red Length:->", tag_1_redLength)

	fmt.Println("tag 2 Green Length:-", tag_2_greenLength)
	fmt.Println("tag 2 Red Length:->", tag_2_redLength)

	fmt.Println("tag 3 Green Length:-", tag_3_greenLength)
	fmt.Println("tag 3 Red Length:->", tag_3_redLength)

	fmt.Println("tag 4 Green Length:-", tag_4_greenLength)
	fmt.Println("tag 4 Red Length:->", tag_4_redLength)

	fmt.Println("tag 5 Green Length:-", tag_5_greenLength)
	fmt.Println("tag 5 Red Length:->", tag_5_redLength)

	Graph := GraphData{
		TAG_1_GreenValue: CalculateDurationForID(tag_1_green_devices),
		TAG_1_GreenLabel: "TAG 1 SAFE ZONE",

		TAG_1_RedValue: CalculateDurationForID(tag_1_red_devices),
		TAG_1_RedLabel: "TAG 1 HAZARD ZONE",

		///////////////////////////////////////////////////////////

		TAG_2_GreenValue: CalculateDurationForID(tag_2_green_devices),
		TAG_2_GreenLabel: "TAG 2 SAFE ZONE",

		TAG_2_RedValue: CalculateDurationForID(tag_2_red_devices),
		TAG_2_RedLabel: "TAG 2 HAZARD ZONE",
		///////////////////////////////////////////////////////////

		TAG_3_GreenValue: CalculateDurationForID(tag_3_green_devices),
		TAG_3_GreenLabel: "TAG 3 SAFE ZONE",

		TAG_3_RedValue: CalculateDurationForID(tag_3_red_devices),
		TAG_3_RedLabel: "TAG 3 HAZARD ZONE",

		///////////////////////////////////////////////////////////

		TAG_4_GreenValue: CalculateDurationForID(tag_4_green_devices),
		TAG_4_GreenLabel: "TAG 4 SAFE ZONE",

		TAG_4_RedValue: CalculateDurationForID(tag_4_red_devices),
		TAG_4_RedLabel: "TAG 4 HAZARD ZONE",

		///////////////////////////////////////////////////////////

	}

	return Graph

	// Respond with the retrieved facilities in JSON format
	//w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(Graph)
}

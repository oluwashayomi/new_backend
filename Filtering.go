package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	//	"time"

	//	"github.com/patrickmn/go-cache"
	"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/bson"
)

// func filterCustomersByTime1(w http.ResponseWriter, r *http.Request) {
// 	filter := r.URL.Query().Get("filter")

// 	fmt.Println("filter", filter)

// 	if cachedData, found := dataCache.Get("ble-data-cache"); found {
// 		// Data found in cache, return it
// 		fmt.Println("Data found in cache!")
// 		w.Header().Set("Content-Type", "application/json")
// 		w.Write(cachedData.([]byte))
// 		return
// 	}

// 	//var filter bson.D

// 	// month := 4   // February
// 	// year := 2024 // 2024

// 	if filter == "April" {
// 		// Fetch all services from MongoDB
// 		month := 4   // February
// 		year := 2024 // 2024
// 		var devices []Device
// 		collection := client.Database(dbName).Collection(devices_colletion)
// 		cursor, err := collection.Find(context.Background(), bson.D{})
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}
// 		defer cursor.Close(context.Background())
// 		for cursor.Next(context.Background()) {
// 			var device Device
// 			err := cursor.Decode(&device)
// 			if err != nil {
// 				http.Error(w, err.Error(), http.StatusInternalServerError)
// 				return
// 			}
// 			devices = append(devices, device)
// 		}

// 		// Convert partners slice to JSON
// 		jsonData, err := json.Marshal(devices)
// 		if err != nil {
// 			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
// 			return
// 		}

// 		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

// 		var filteredObjects []Device
// 		for _, obj := range devices {
// 			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
// 			if err != nil {
// 				fmt.Println("Error parsing time:", err)
// 				continue
// 			}

// 			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
// 				filteredObjects = append(filteredObjects, obj)
// 			}
// 		}

// 		handleGetGraphsFunc(filteredObjects)

// 		// Respond with the retrieved services
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(filteredObjects)
// 		return
// 	} else if filter == "March" {

// 		fmt.Println("March passed")

// 		// Fetch all services from MongoDB
// 		month := 3   // February
// 		year := 2024 // 2024
// 		var devices []Device
// 		collection := client.Database(dbName).Collection(devices_colletion)
// 		cursor, err := collection.Find(context.Background(), bson.D{})
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}
// 		defer cursor.Close(context.Background())
// 		for cursor.Next(context.Background()) {
// 			var device Device
// 			err := cursor.Decode(&device)
// 			if err != nil {
// 				http.Error(w, err.Error(), http.StatusInternalServerError)
// 				return
// 			}
// 			devices = append(devices, device)
// 		}

// 		// Convert partners slice to JSON
// 		jsonData, err := json.Marshal(devices)
// 		if err != nil {
// 			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
// 			return
// 		}

// 		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

// 		var filteredObjects []Device
// 		for _, obj := range devices {
// 			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
// 			if err != nil {
// 				fmt.Println("Error parsing time:", err)
// 				continue
// 			}

// 			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
// 				filteredObjects = append(filteredObjects, obj)
// 			}
// 		}

// 		// Respond with the retrieved services
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(filteredObjects)
// 		return

// 	} else if filter == "February" {

// 		fmt.Println("February passed")

// 		// Fetch all services from MongoDB
// 		month := 2   // February
// 		year := 2024 // 2024
// 		var devices []Device
// 		collection := client.Database(dbName).Collection(devices_colletion)
// 		cursor, err := collection.Find(context.Background(), bson.D{})
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}
// 		defer cursor.Close(context.Background())
// 		for cursor.Next(context.Background()) {
// 			var device Device
// 			err := cursor.Decode(&device)
// 			if err != nil {
// 				http.Error(w, err.Error(), http.StatusInternalServerError)
// 				return
// 			}
// 			devices = append(devices, device)
// 		}

// 		// Convert partners slice to JSON
// 		jsonData, err := json.Marshal(devices)
// 		if err != nil {
// 			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
// 			return
// 		}

// 		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

// 		var filteredObjects []Device
// 		for _, obj := range devices {
// 			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
// 			if err != nil {
// 				fmt.Println("Error parsing time:", err)
// 				continue
// 			}

// 			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
// 				filteredObjects = append(filteredObjects, obj)
// 			}
// 		}

// 		// // Respond with the retrieved facilities in JSON format
// 		// w.Header().Set("Content-Type", "application/json")
// 		// json.NewEncoder(w).Encode(devices)

// 		// Respond with the retrieved services
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(filteredObjects)
// 		return

// 	} else if filter == "January" {

// 		fmt.Println("February passed")

// 		// Fetch all services from MongoDB
// 		month := 1   // February
// 		year := 2024 // 2024
// 		var devices []Device
// 		collection := client.Database(dbName).Collection(devices_colletion)
// 		cursor, err := collection.Find(context.Background(), bson.D{})
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}
// 		defer cursor.Close(context.Background())
// 		for cursor.Next(context.Background()) {
// 			var device Device
// 			err := cursor.Decode(&device)
// 			if err != nil {
// 				http.Error(w, err.Error(), http.StatusInternalServerError)
// 				return
// 			}
// 			devices = append(devices, device)
// 		}

// 		// Convert partners slice to JSON
// 		jsonData, err := json.Marshal(devices)
// 		if err != nil {
// 			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
// 			return
// 		}

// 		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

// 		var filteredObjects []Device
// 		for _, obj := range devices {
// 			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
// 			if err != nil {
// 				fmt.Println("Error parsing time:", err)
// 				continue
// 			}

// 			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
// 				filteredObjects = append(filteredObjects, obj)
// 			}
// 		}

// 		// // Respond with the retrieved facilities in JSON format
// 		// w.Header().Set("Content-Type", "application/json")
// 		// json.NewEncoder(w).Encode(devices)

// 		// Respond with the retrieved services
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(filteredObjects)
// 		return

// 	} else if filter == "24Hours" {

// 		fmt.Println("February passed")

// 		// Fetch all services from MongoDB
// 		//month := 1   // February
// 		//year := 2024 // 2024
// 		var devices []Device
// 		collection := client.Database(dbName).Collection(devices_colletion)
// 		cursor, err := collection.Find(context.Background(), bson.D{})
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}
// 		defer cursor.Close(context.Background())
// 		for cursor.Next(context.Background()) {
// 			var device Device
// 			err := cursor.Decode(&device)
// 			if err != nil {
// 				http.Error(w, err.Error(), http.StatusInternalServerError)
// 				return
// 			}
// 			devices = append(devices, device)
// 		}

// 		// Convert partners slice to JSON
// 		jsonData, err := json.Marshal(devices)
// 		if err != nil {
// 			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
// 			return
// 		}

// 		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

// 		var filteredObjects []Device
// 		for _, obj := range devices {
// 			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
// 			if err != nil {
// 				fmt.Println("Error parsing time:", err)
// 				continue
// 			}

// 			if time.Since(createdAt) <= 24*time.Hour {
// 				filteredObjects = append(filteredObjects, obj)
// 			}
// 		}

// 		// // Respond with the retrieved facilities in JSON format
// 		// w.Header().Set("Content-Type", "application/json")
// 		// json.NewEncoder(w).Encode(devices)

// 		// Respond with the retrieved services
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(filteredObjects)
// 		return

// 	}

// }

func filterCustomersByTimeGraph(w http.ResponseWriter, r *http.Request) {
	filter := r.URL.Query().Get("filter")

	fmt.Println("filter", filter)
	// if cachedData, found := dataCache.Get("ble-data-cache"); found {
	// 	// Data found in cache, return it
	// 	fmt.Println("Data found in cache!")
	// 	w.Header().Set("Content-Type", "application/json")
	// 	w.Write(cachedData.([]byte))
	// 	return
	// }

	if filter == "April" {
		// Fetch all services from MongoDB
		month := 4   // February
		year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		//handleGetGraphsFunc(filteredObjects)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(handleGetGraphsFunc(filteredObjects))
		return
	} else if filter == "March" {

		fmt.Println("March passed")

		// Fetch all services from MongoDB
		month := 3   // February
		year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(handleGetGraphsFunc(filteredObjects))
		return

	} else if filter == "February" {

		fmt.Println("February passed")

		// Fetch all services from MongoDB
		month := 2   // February
		year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(devices)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(handleGetGraphsFunc(filteredObjects))
		return

	} else if filter == "January" {

		fmt.Println("February passed")

		// Fetch all services from MongoDB
		month := 1   // February
		year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(devices)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(handleGetGraphsFunc(filteredObjects))
		return

	} else if filter == "24Hours" {

		fmt.Println("February passed")

		// Fetch all services from MongoDB
		//month := 1   // February
		//year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if time.Since(createdAt) <= 24*time.Hour {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(devices)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(handleGetGraphsFunc(filteredObjects))
		return

	} else if filter == "ALL" {

		fmt.Println("February passed")

		// Fetch all services from MongoDB
		//month := 1   // February
		//year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		// var filteredObjects []Device
		// for _, obj := range devices {
		// 	createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
		// 	if err != nil {
		// 		fmt.Println("Error parsing time:", err)
		// 		continue
		// 	}

		// 	if time.Since(createdAt) <= 24*time.Hour {
		// 		filteredObjects = append(filteredObjects, obj)
		// 	}
		// }

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(devices)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(handleGetGraphsFunc(devices))
		return

	}

}

func filterCustomersByTime_Data(w http.ResponseWriter, r *http.Request) {
	filter := r.URL.Query().Get("filter")

	fmt.Println("filter", filter)
	//fmt.Println("filter", filter)
	// if cachedData, found := dataCache.Get("ble-data-cache"); found {
	// 	// Data found in cache, return it
	// 	fmt.Println("Data found in cache!")
	// 	w.Header().Set("Content-Type", "application/json")
	// 	w.Write(cachedData.([]byte))
	// 	return
	// }

	if filter == "April" {
		// Fetch all services from MongoDB
		// if cachedData, found := dataCache.Get("ble-data-cache"); found {
		// 	// Data found in cache, return it
		// 	fmt.Println("Data found in cache!")
		// 	w.Header().Set("Content-Type", "application/json")
		// 	w.Write(cachedData.([]byte))
		// 	return
		// }
		month := 4   // February
		year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		//handleGetGraphsFunc(filteredObjects)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(filteredObjects)
		return
	} else if filter == "March" {

		fmt.Println("March passed")

		// Fetch all services from MongoDB
		month := 3   // February
		year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(filteredObjects)
		return

	} else if filter == "February" {

		fmt.Println("February passed")

		// Fetch all services from MongoDB
		month := 2   // February
		year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(devices)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(filteredObjects)
		return

	} else if filter == "January" {

		fmt.Println("February passed")

		// Fetch all services from MongoDB
		month := 1   // February
		year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if createdAt.Month() == time.Month(month) && createdAt.Year() == year {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(devices)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(filteredObjects)
		return

	} else if filter == "24Hours" {

		fmt.Println("February passed")

		// Fetch all services from MongoDB
		//month := 1   // February
		//year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		var filteredObjects []Device
		for _, obj := range devices {
			createdAt, err := time.Parse("1/2/2006, 3:04:05 PM", obj.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}

			if time.Since(createdAt) <= 24*time.Hour {
				filteredObjects = append(filteredObjects, obj)
			}
		}

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(devices)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(filteredObjects)
		return

	} else if filter == "ALL" {

		fmt.Println("February passed")

		// Fetch all services from MongoDB
		//month := 1   // February
		//year := 2024 // 2024
		var devices []Device
		collection := client.Database(dbName).Collection(devices_colletion)
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		for cursor.Next(context.Background()) {
			var device Device
			err := cursor.Decode(&device)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			devices = append(devices, device)
		}

		// Convert partners slice to JSON
		jsonData, err := json.Marshal(devices)
		if err != nil {
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
			return
		}

		dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

		// Respond with the retrieved services
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(devices)
		return

	}

}

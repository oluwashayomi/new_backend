package main

import (
	"fmt"
	"math"
	"time"
)

func ParseTime(timeStr string) (time.Time, error) {
	layout := "1/2/2006, 3:04:05 PM"
	return time.Parse(layout, timeStr)
}

// CalculateDurationForID calculates the total duration for a specific ID
func CalculateDurationForID(logEntries []Device) float64 {
	var totalDuration time.Duration

	var lastTimestamp time.Time
	for _, entry := range logEntries {
	//	if entry.ID == id {
			timestamp, err := ParseTime(entry.CreatedAt)
			if err != nil {
				fmt.Println("Error parsing time:", err)
				continue
			}
			if !lastTimestamp.IsZero() {
				duration := timestamp.Sub(lastTimestamp)
				totalDuration += duration
			}
			lastTimestamp = timestamp
		//}
	}

	// Convert total duration from nanoseconds to hours
	totalHours := totalDuration.Hours()

	// Round the result to two decimal places
	totalHoursRounded := math.Round(totalHours*100) / 100
	return totalHoursRounded
}

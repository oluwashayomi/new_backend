package main

import (
	"context"
///	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	//"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/bson"
)

func handleGetDevices2(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get devices!!!----")

	status := r.URL.Query().Get("status")

	fmt.Println("Status", status)

	var filter bson.D

	if status == "ALL" {
		// g := handleGetGraphsFunc()

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(g)
		fmt.Println("Get devices!!!----")
		collection := client.Database(dbName).Collection(devices_colletion)

		// Define a slice to store retrieved facilities
		//var devices_green []Device

		//var devices_red []Device

		//var tag_1_devices []Device
		var tag_1_red_devices []Device
		var tag_1_green_devices []Device

		var tag_2_red_devices []Device
		var tag_2_green_devices []Device

		var tag_3_red_devices []Device
		var tag_3_green_devices []Device

		var tag_4_red_devices []Device
		var tag_4_green_devices []Device

		var tag_5_red_devices []Device
		var tag_5_green_devices []Device

		//var graphData []GraphData

		// Retrieve all documents from the collection
		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, "Error retrieving devices", http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())

		// Iterate through the cursor and decode documents into the facilities slice
		for cursor.Next(context.Background()) {
			var device Device
			if err := cursor.Decode(&device); err != nil {
				http.Error(w, "Error decoding device", http.StatusInternalServerError)
				return
			}
			if device.Name != "" && device.Name != "NaN" {

				//fmt.Println("TAG-", device.Name)

				if device.Name == "TAG-1:" {
					//fmt.Println("TAG-1", device)
					//tag_1_devices = append(tag_1_devices, device)

					// Check if RSSI value is not empty or "NaN"
					if device.Rssi != "" && device.Rssi != "NaN" {
						// Convert RSSI string to integer
						rssi, err := strconv.Atoi(device.Rssi)
						if err != nil {
							fmt.Println("ERR", err)
							http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
							return
						}

						// Filter devices based on RSSI values
						if rssi <= -39 && rssi >= -50 {
							tag_1_red_devices = append(tag_1_red_devices, device)
						} else if rssi < -50 {
							tag_1_green_devices = append(tag_1_green_devices, device)
						}
					}

				} else if device.Name == "TAG-2:" {
					//fmt.Println("TAG-2", device)
					//tag_1_devices = append(tag_1_devices, device)

					// Check if RSSI value is not empty or "NaN"
					if device.Rssi != "" && device.Rssi != "NaN" {
						// Convert RSSI string to integer
						rssi, err := strconv.Atoi(device.Rssi)
						if err != nil {
							fmt.Println("ERR", err)
							http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
							return
						}

						// Filter devices based on RSSI values
						if rssi <= -39 && rssi >= -50 {
							tag_2_red_devices = append(tag_2_red_devices, device)
						} else if rssi < -50 {
							tag_2_green_devices = append(tag_2_green_devices, device)
						}
					}

				} else if device.Name == "TAG-3:" {
					//fmt.Println("TAG-3", device)
					//tag_1_devices = append(tag_1_devices, device)

					// Check if RSSI value is not empty or "NaN"
					if device.Rssi != "" && device.Rssi != "NaN" {
						// Convert RSSI string to integer
						rssi, err := strconv.Atoi(device.Rssi)
						if err != nil {
							fmt.Println("ERR", err)
							http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
							return
						}

						// Filter devices based on RSSI values
						if rssi <= -39 && rssi >= -50 {
							tag_3_red_devices = append(tag_3_red_devices, device)
						} else if rssi < -50 {
							tag_3_green_devices = append(tag_3_green_devices, device)
						}
					}

				} else if device.Name == "TAG-4:" {
					//fmt.Println("TAG-4", device)
					//tag_1_devices = append(tag_1_devices, device)

					// Check if RSSI value is not empty or "NaN"
					if device.Rssi != "" && device.Rssi != "NaN" {
						// Convert RSSI string to integer
						rssi, err := strconv.Atoi(device.Rssi)
						if err != nil {
							fmt.Println("ERR", err)
							http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
							return
						}

						// Filter devices based on RSSI values
						if rssi <= -39 && rssi >= -50 {
							tag_4_red_devices = append(tag_4_red_devices, device)
						} else if rssi < -50 {
							tag_4_green_devices = append(tag_4_green_devices, device)
						}
					}

				} else if device.Name == "TAG-5:" {
					//fmt.Println("TAG-5", device)
					//tag_1_devices = append(tag_1_devices, device)

					// Check if RSSI value is not empty or "NaN"
					if device.Rssi != "" && device.Rssi != "NaN" {
						// Convert RSSI string to integer
						rssi, err := strconv.Atoi(device.Rssi)
						if err != nil {
							fmt.Println("ERR", err)
							http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
							return
						}

						// Filter devices based on RSSI values
						if rssi <= -39 && rssi >= -50 {
							tag_5_red_devices = append(tag_5_red_devices, device)
						} else if rssi < -50 {
							tag_5_green_devices = append(tag_5_green_devices, device)
						}
					}

				}

			}

			//devices = append(devices, device)

			// Get the length of the slices

		}

		tag_1_greenLength := len(tag_1_green_devices)
		tag_1_redLength := len(tag_1_red_devices)

		tag_2_greenLength := len(tag_2_red_devices)
		tag_2_redLength := len(tag_2_green_devices)

		tag_3_greenLength := len(tag_3_red_devices)
		tag_3_redLength := len(tag_3_green_devices)

		tag_4_greenLength := len(tag_4_red_devices)
		tag_4_redLength := len(tag_4_green_devices)

		tag_5_greenLength := len(tag_5_red_devices)
		tag_5_redLength := len(tag_5_green_devices)

		fmt.Println("tag 1 Green Length:-", tag_1_greenLength)
		fmt.Println("tag 1 Red Length:->", tag_1_redLength)

		fmt.Println("tag 2 Green Length:-", tag_2_greenLength)
		fmt.Println("tag 2 Red Length:->", tag_2_redLength)

		fmt.Println("tag 3 Green Length:-", tag_3_greenLength)
		fmt.Println("tag 3 Red Length:->", tag_3_redLength)

		fmt.Println("tag 4 Green Length:-", tag_4_greenLength)
		fmt.Println("tag 4 Red Length:->", tag_4_redLength)

		fmt.Println("tag 5 Green Length:-", tag_5_greenLength)
		fmt.Println("tag 5 Red Length:->", tag_5_redLength)

		// Graph := GraphData{
		// 	TAG_1_GreenValue: tag_1_greenLength,
		// 	TAG_1_GreenLabel: "tag 1 GREEN",

		// 	TAG_1_RedValue: tag_1_redLength,
		// 	TAG_1_RedLabel: "tag 1 RED",

		// 	///////////////////////////////////////////////////////////

		// 	TAG_2_GreenValue: tag_2_greenLength,
		// 	TAG_2_GreenLabel: "TAG 2 GREEN",

		// 	TAG_2_RedValue: tag_2_redLength,
		// 	TAG_2_RedLabel: "TAG 2 RED",
		// 	///////////////////////////////////////////////////////////

		// 	TAG_3_GreenValue: tag_3_greenLength,
		// 	TAG_3_GreenLabel: "TAG 3 GREEN",

		// 	TAG_3_RedValue: tag_3_redLength,
		// 	TAG_3_RedLabel: "TAG 3 RED",

		// 	///////////////////////////////////////////////////////////

		// 	TAG_4_GreenValue: tag_4_greenLength,
		// 	TAG_4_GreenLabel: "TAG 4 GREEN",

		// 	TAG_4_RedValue: tag_4_redLength,
		// 	TAG_4_RedLabel: "TAG 4 RED",

		// 	///////////////////////////////////////////////////////////

		// }

		// // Respond with the retrieved facilities in JSON format
		// w.Header().Set("Content-Type", "application/json")
		// json.NewEncoder(w).Encode(Graph)

		return

	}

	// Check status value and set filter accordingly
	switch status {
	case "LAST_HOUR":
		// Calculate the time one hour ago
		layout := "1/2/2006, 3:04:05 PM"
		oneHourAgo := time.Now().Add(-time.Hour)
		oneHourAgoStr := oneHourAgo.Format(layout)

		fmt.Println("one hour ago", oneHourAgo)
		filter = bson.D{{"created_at", bson.D{{"$gte", oneHourAgoStr}}}}

		fmt.Println("canges")
	case "LAST_24_HOURS":
		// Calculate the time 24 hours ago
		layout := "1/2/2006, 3:04:05 PM"
		twentyFourHoursAgo := time.Now().Add(-24 * time.Hour)
		twentyFourHoursAgoStr := twentyFourHoursAgo.Format(layout)
		// twentyFourHoursAgo := time.Now().Add(-24 * time.Hour)
		fmt.Println("24hours ", twentyFourHoursAgoStr)
		filter = bson.D{{"created_at", bson.D{{"$gte", twentyFourHoursAgoStr}}}}
	case "LAST_WEEK":
		// Calculate the start time for exactly one week ago
		//oneWeekAgo := time.Now().AddDate(0, 0, -7)

		layout := "1/2/2006, 3:04:05 PM"
		oneWeekAgo := time.Now().AddDate(0, 0, -7)
		oneWeekAgoStr := oneWeekAgo.Format(layout)
		// twentyFourHoursAgo := time.Now().Add(-24 * time.Hour)
		fmt.Println("1 week ago ", oneWeekAgoStr)
		filter = bson.D{{"created_at", bson.D{{"$gt", oneWeekAgoStr}}}}
	case "LAST_MONTH":
		// Calculate the start time for the beginning of the current month
		now := time.Now()
		startOfMonth := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location())
		filter = bson.D{{"created_at", bson.D{{"$gte", startOfMonth.Format("1/2/2006, 03:04:05 PM")}}}}

	case "LAST_3_MONTHS":
		// Calculate the start time for exactly three months ago
		threeMonthsAgo := time.Now().AddDate(0, -3, 0)
		filter = bson.D{{"created_at", bson.D{{"$gte", threeMonthsAgo.Format("1/2/2006, 03:04:05 PM")}}}}
	default:
		http.Error(w, "Invalid status value", http.StatusBadRequest)
		return
	}

	// if cachedData, found := dataCache.Get("ble-data-cache"); found {
	// 	// Data found in cache, return it
	// 	fmt.Println("Data found in cache!")
	// 	w.Header().Set("Content-Type", "application/json")
	// 	w.Write(cachedData.([]byte))
	// 	return
	// }
	//collection := client.Database(dbName).Collection(devices_colletion)

	var devices []Device

	var tag_1_red_devices []Device
	var tag_1_green_devices []Device

	var tag_2_red_devices []Device
	var tag_2_green_devices []Device

	var tag_3_red_devices []Device
	var tag_3_green_devices []Device

	var tag_4_red_devices []Device
	var tag_4_green_devices []Device

	var tag_5_red_devices []Device
	var tag_5_green_devices []Device

	collection := client.Database(dbName).Collection(devices_colletion)
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var device Device
		if err := cursor.Decode(&device); err != nil {
			http.Error(w, "Error decoding device", http.StatusInternalServerError)
			return
		}

		fmt.Println("filtered device", device)
		if device.Name != "" && device.Name != "NaN" {

			//fmt.Println("TAG-", device.Name)

			if device.Name == "TAG-1:" {
				fmt.Println("TAG-1", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_1_red_devices = append(tag_1_red_devices, device)
					} else if rssi < -50 {
						tag_1_green_devices = append(tag_1_green_devices, device)
					}
				}

			} else if device.Name == "TAG-2:" {
				//fmt.Println("TAG-2", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_2_red_devices = append(tag_2_red_devices, device)
					} else if rssi < -50 {
						tag_2_green_devices = append(tag_2_green_devices, device)
					}
				}

			} else if device.Name == "TAG-3:" {
				//fmt.Println("TAG-3", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_3_red_devices = append(tag_3_red_devices, device)
					} else if rssi < -50 {
						tag_3_green_devices = append(tag_3_green_devices, device)
					}
				}

			} else if device.Name == "TAG-4:" {
				//fmt.Println("TAG-4", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_4_red_devices = append(tag_4_red_devices, device)
					} else if rssi < -50 {
						tag_4_green_devices = append(tag_4_green_devices, device)
					}
				}

			} else if device.Name == "TAG-5:" {
				//fmt.Println("TAG-5", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_5_red_devices = append(tag_5_red_devices, device)
					} else if rssi < -50 {
						tag_5_green_devices = append(tag_5_green_devices, device)
					}
				}

			}

		}

		//devices = append(devices, device)

		fmt.Println("devices", devices)

		// Get the length of the slices

	}

	tag_1_greenLength := len(tag_1_green_devices)
	tag_1_redLength := len(tag_1_red_devices)

	tag_2_greenLength := len(tag_2_red_devices)
	tag_2_redLength := len(tag_2_green_devices)

	tag_3_greenLength := len(tag_3_red_devices)
	tag_3_redLength := len(tag_3_green_devices)

	tag_4_greenLength := len(tag_4_red_devices)
	tag_4_redLength := len(tag_4_green_devices)

	tag_5_greenLength := len(tag_5_red_devices)
	tag_5_redLength := len(tag_5_green_devices)

	fmt.Println("tag 1 Green Length:-", tag_1_greenLength)
	fmt.Println("tag 1 Red Length:->", tag_1_redLength)

	fmt.Println("tag 2 Green Length:-", tag_2_greenLength)
	fmt.Println("tag 2 Red Length:->", tag_2_redLength)

	fmt.Println("tag 3 Green Length:-", tag_3_greenLength)
	fmt.Println("tag 3 Red Length:->", tag_3_redLength)

	fmt.Println("tag 4 Green Length:-", tag_4_greenLength)
	fmt.Println("tag 4 Red Length:->", tag_4_redLength)

	fmt.Println("tag 5 Green Length:-", tag_5_greenLength)
	fmt.Println("tag 5 Red Length:->", tag_5_redLength)

	// Graph := GraphData{
	// 	TAG_1_GreenValue: tag_1_greenLength,
	// 	TAG_1_GreenLabel: "tag 1 GREEN",

	// 	TAG_1_RedValue: tag_1_redLength,
	// 	TAG_1_RedLabel: "tag 1 RED",

	// 	///////////////////////////////////////////////////////////

	// 	TAG_2_GreenValue: tag_2_greenLength,
	// 	TAG_2_GreenLabel: "TAG 2 GREEN",

	// 	TAG_2_RedValue: tag_2_redLength,
	// 	TAG_2_RedLabel: "TAG 2 RED",
	// 	///////////////////////////////////////////////////////////

	// 	TAG_3_GreenValue: tag_3_greenLength,
	// 	TAG_3_GreenLabel: "TAG 3 GREEN",

	// 	TAG_3_RedValue: tag_3_redLength,
	// 	TAG_3_RedLabel: "TAG 3 RED",

	// 	///////////////////////////////////////////////////////////

	// 	TAG_4_GreenValue: tag_4_greenLength,
	// 	TAG_4_GreenLabel: "TAG 4 GREEN",

	// 	TAG_4_RedValue: tag_4_redLength,
	// 	TAG_4_RedLabel: "TAG 4 RED",

	// 	///////////////////////////////////////////////////////////

	// }

	// // Respond with the retrieved facilities in JSON format
	// w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(Graph)
}
